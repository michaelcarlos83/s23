
function Pokemon(pokemon){
	//properties
	this.name = "Ash Ketchum";
	this.age = 10;

	this.pokemon = ['pikachu', 'charizard', 'squirtle', 'bulbasaur'];
	this.friends = {
		hoenn:['May', 'Max'],
		kanto:['Brock', 'Misty']
	};

	this.talk = function(target){
		if(pokemon[0]){
			console.log("Pikachu I choose you!")
		}
		else if (pokemon[1]){
			console.log("Charizard I choose you!")
		}
		else if(pokemon[2]){
			console.log("Squirtle I choose you!")
		}
		else if(pokemon[3]){
			console.log("Bulbasaur I choose you!")
		}else{
			console.log("Please enter a valid response")
		}
	}

}





function PokemonAttributes(name, level, health, attack){

	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;

	//methods

	this.tackle = function(target){
		
		let targetHealth = target.health - this.attack;
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(targetHealth));

		if(targetHealth < 1){
			console.log(target.name + " fainted")
		}

	}

}


let pikachu = new PokemonAttributes("Pikachu", 12, 24, 12)
let geodude = new PokemonAttributes("Geodude", 8, 16, 8)
let mewtwo = new PokemonAttributes("Mewtwo", 100, 200, 100)



mewtwo.tackle(geodude);







